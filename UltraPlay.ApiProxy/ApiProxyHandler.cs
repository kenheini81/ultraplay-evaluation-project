﻿using UltraPlay.Data.Models;
using UltraPlay.Data.Repositories;
using UltraPlay.Serialization;

namespace UltraPlay.ApiProxy
{
    public interface IApiProxyHandler : IDependency
    {
        void OnDataDownloaded(string eventResult);
    }

    public class ApiProxyHandler : IApiProxyHandler
    {
        private readonly IXmlSerializationHelper _xmlSerializationHelper;
        private readonly IRepository<Sport> _sportRepository;
        private readonly IRepository<Event> _eventRepository;
        private readonly IRepository<Match> _matchRepository;
        private readonly IRepository<Bet> _betRepository;
        private readonly IRepository<Odd> _oddRepository;

        public ApiProxyHandler(IXmlSerializationHelper xmlSerializationHelper,
                               IRepository<Sport> sportRepository,
                               IRepository<Event> eventRepository,
                               IRepository<Match> matchRepository,
                               IRepository<Bet> betRepository,
                               IRepository<Odd> oddRepository)
        {
            _xmlSerializationHelper = xmlSerializationHelper;
            _sportRepository = sportRepository;
            _eventRepository = eventRepository;
            _matchRepository = matchRepository;
            _betRepository = betRepository;
            _oddRepository = oddRepository;
        }

        public virtual void OnDataDownloaded(string eventResult)
        {
            var xmlSportsData = ParseData(eventResult);

            _sportRepository.DeleteByCondition(x => true);
            _sportRepository.SaveChanges();


            //foreach (var sport in xmlSportsData.Sports)
            //{
            //    InsertOrUpdateSport(sport);
            //}

            foreach (var sport in xmlSportsData.Sports)
            {
                _sportRepository.Insert(sport);
            }
            _sportRepository.SaveChanges();
        }

        public virtual XmlSport ParseData(string eventXmlData)
        {
            var output = _xmlSerializationHelper.Deserialized<XmlSport>(eventXmlData);

            return output;
        }

        private void InsertOrUpdateSport(Sport sport)
        {
            var dbSport = _sportRepository.GetById(sport.Id);

            if (dbSport == null)
            {
                _sportRepository.Insert(sport);
            }
            else
            {
                dbSport.Name = sport.Name;
                _sportRepository.Update(dbSport);

                foreach (var evt in sport.Events)
                {
                    InsertOrUpdateEvent(evt, dbSport);
                }
            }

            // cuz IDbContext is singleton for each request, calling this will save everything
            _sportRepository.SaveChanges();
        }

        private void InsertOrUpdateEvent(Event evt, Sport sport)
        {
            var dbEvent = _eventRepository.GetById(evt.Id);
            if (dbEvent == null)
            {
                evt.Sport = sport;
                _eventRepository.Insert(evt);
            }
            else
            {
                dbEvent.CategoryId = evt.CategoryId;
                dbEvent.Name = evt.Name;
                dbEvent.IsLive = evt.IsLive;
                _eventRepository.Update(dbEvent);
                foreach (var match in evt.Matches)
                {
                    InsertOrUpdateMatch(match, dbEvent);
                }
            }
        }

        private void InsertOrUpdateMatch(Match match, Event dbEvent)
        {
            var dbMatch = _matchRepository.GetById(match.Id);
            if (dbMatch == null)
            {
                match.Event = dbEvent;
                _matchRepository.Insert(match);
            }
            else
            {
                dbMatch.Name = match.Name;
                dbMatch.StartDate = match.StartDate;
                dbMatch.MatchType = match.MatchType;
                _matchRepository.Update(dbMatch);
                foreach (var bet in dbMatch.Bets)
                {
                    InsertOrUpdateBet(bet, dbMatch);
                }
            }
        }

        private void InsertOrUpdateBet(Bet bet, Match dbMatch)
        {
            var dbBet = _betRepository.GetById(bet.Id);
            if (dbBet == null)
            {
                bet.Match = dbMatch;
                _betRepository.Insert(bet);
            }
            else
            {
                dbBet.Name = bet.Name;
                dbBet.IsLive = bet.IsLive;
                _betRepository.Update(dbBet);

                foreach (var odd in bet.Odds)
                {
                    InsertOrUpdateOdd(odd, dbBet);
                }
            }
        }

        private void InsertOrUpdateOdd(Odd odd, Bet dbBet)
        {
            var dbOdd = _oddRepository.GetById(odd.Id);
            if (dbOdd == null)
            {
                odd.Bet = dbBet;
                _oddRepository.Insert(odd);
            }
            else
            {
                dbOdd.Name = odd.Name;
                dbOdd.Value = odd.Value;
                _oddRepository.Update(dbOdd);
            }
        }
    }
}