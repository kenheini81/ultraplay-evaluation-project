﻿using System;
using System.Net;
using System.Threading.Tasks;

namespace UltraPlay.ApiProxy
{
    public interface IApiProxy : IDependency
    {
        Task<string> GetDataAsync();
        void GetData();
    }

    public class ApiProxy : IApiProxy
    {
        const string Url = "http://vitalbet.com/sportxml";

        private readonly IApiProxyHandler _apiProxyHandler;

        public ApiProxy(IApiProxyHandler apiProxyHandler)
        {
            _apiProxyHandler = apiProxyHandler;
        }

        public async Task<string> GetDataAsync()
        {
            var wc = new WebClient();
            wc.Headers.Add("Accept", "application/xml");
            wc.DownloadStringCompleted += (sender, args) => OnDataDownloaded(args.Result);
            return await wc.DownloadStringTaskAsync(new Uri(Url));
        }

        public void GetData()
        {
            var result = GetDataAsync().Result;
            // just make sure it runs synchronously
        }

        private void OnDataDownloaded(string result)
        {
            _apiProxyHandler.OnDataDownloaded(result);
        }
    }
}
