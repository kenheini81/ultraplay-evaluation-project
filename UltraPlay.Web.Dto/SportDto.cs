﻿using System.Collections.ObjectModel;

namespace UltraPlay.Web.Dto
{
    public class SportDto
    {
        public string Name { get; set; }

        public long Id { get; set; }
    }
    public class SportWithEventDto : SportDto
    {
        public virtual Collection<EventBaseDto> Events { get; set; }
    }
}