﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace UltraPlay.Data.Models
{
    [DataContract]
    public class Odd
    {
        [XmlAttribute(AttributeName = "Name"), JsonProperty]
        public string Name { get; set; }

        [XmlAttribute(AttributeName = "ID"), JsonProperty]
        [DatabaseGenerated(DatabaseGeneratedOption.None), Key]
        public long Id { get; set; }

        [XmlAttribute(AttributeName = "Value"), JsonProperty]
        public double Value { get; set; }

        [XmlIgnore, JsonIgnore]
        public virtual Bet Bet { get; set; }
    }
}