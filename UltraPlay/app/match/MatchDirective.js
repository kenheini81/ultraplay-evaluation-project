/**
 * Created on 8/7/2015.
 */
import './MatchDetailController';

angular.module('ultraplay')
    .directive('matchDetail', function () {
        return {
            restrict: "EAC",
            controller: 'MatchDetailController',
            scope: {
                matchId: '=matchDetail'
            },
            controllerAs: "match",
            templateUrl: "app/match/match.tpl.html"
        };
    });