﻿import './SportService';

class SportController {
    constructor(SportService, $stateParams, $state) {
        if (!$stateParams.sport_id) $state.go("home");
        this.data = [];
        this.sportName = "";
        this.sportId = $stateParams.sport_id;
        this.SportService = SportService;
        this.$state = $state;
        this.refresh();
    }

    refresh() {
        this.SportService.getSport(this.sportId, this.refreshSportData.bind(this));
    }

    goToEvent(event) {
        console.log(event);
        this.$state.go('sport_detail.event_detail', {eventId: event.Id});
    }

    refreshSportData(data) {
        this.sportName = data.Name;
        this.data = data.Events;
    }
}

SportController.$inject = ['SportService', '$stateParams', '$state'];

angular.module('ultraplay')
    .controller('SportController', SportController);