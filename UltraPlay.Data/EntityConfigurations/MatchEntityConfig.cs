﻿using System.Data.Entity.ModelConfiguration;
using UltraPlay.Data.Models;

namespace UltraPlay.Data.EntityConfigurations
{
    public class MatchEntityConfig : EntityTypeConfiguration<Match>
    {
        public MatchEntityConfig()
        {
            ToTable("Matches");

            HasKey(match => match.Id);

            HasMany(match => match.Bets)
                .WithRequired(bet => bet.Match)
                .WillCascadeOnDelete(true);
        }
    }
}