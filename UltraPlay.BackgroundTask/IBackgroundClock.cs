﻿using Autofac;
using System;
using System.Configuration;
using System.Timers;
using UltraPlay.Contexts;

namespace UltraPlay.BackgroundTask
{
    public interface IBackgroundClock : ISingletonDependency
    {
        TimeSpan Interval { get; set; }
        void Activate();
        void Terminate();
    }

    public class BackgroundClock : Component, IBackgroundClock, IDisposable
    {
        private readonly UltraPlayBackgroundContext _backgroundContext;
        private readonly Timer _timer;

        public BackgroundClock(UltraPlayBackgroundContext backgroundContext)
        {
            _backgroundContext = backgroundContext;
            _timer = new Timer();
            _timer.Elapsed += Elapsed;

            double interval;

            if (!double.TryParse(ConfigurationManager.AppSettings["PollingVitalBetApiInterval"], out interval))
            {
                interval = 60;
            }

            Interval = TimeSpan.FromSeconds(interval);
        }

        public TimeSpan Interval
        {
            get { return TimeSpan.FromMilliseconds(_timer.Interval); }
            set { _timer.Interval = value.TotalMilliseconds; }
        }

        public void Activate()
        {
            lock (_timer)
            {
                _timer.Start();
            }
        }

        public void Terminate()
        {
            lock (_timer)
            {
                _timer.Stop();
            }
        }

        void Elapsed(object sender, ElapsedEventArgs e)
        {
            // disallows re-entrancy if there is any thread running
            if (!System.Threading.Monitor.TryEnter(_timer))
                return;

            try
            {
                if (_timer.Enabled)
                {
                    DoWork();
                }
            }
            catch (Exception ex)
            {
                Logger.Warn(ex, "Problem in background tasks");
            }
            finally
            {
                System.Threading.Monitor.Exit(_timer);
            }
        }

        private void DoWork()
        {
            using (var scope = _backgroundContext.SimulateWebRequestScope())
            {
                var manager = scope.Resolve<IBackgroundTaskRunner>();
                manager.DoWork();
            }
        }

        public void Dispose()
        {
            _timer.Dispose();
        }
    }
}