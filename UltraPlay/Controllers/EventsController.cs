﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UltraPlay.Data.Models;
using UltraPlay.Data.Repositories;
using UltraPlay.ObjectMappings;
using UltraPlay.Web.Dto;
using WebGrease.Css.Extensions;

namespace UltraPlay.Web.Controllers
{
    public class EventsController : ApiController
    {
        private readonly IRepository<Event> _eventRepository;
        private readonly IMappingService _mappingService;

        public EventsController(IRepository<Event> eventRepository, IMappingService mappingService)
        {
            _eventRepository = eventRepository;
            _mappingService = mappingService;
        }

        [Route("api/events")]
        public HttpResponseMessage GetAllEvents(bool? live = null)
        {
            var events = _eventRepository.GetAll();

            if (live != null && live.Value)
            {
                events = events.Where(x => x.IsLive);
            }

            var outputEvents = _mappingService.Map<IEnumerable<EventBaseDto>>(events.ToArray());

            return Request.CreateResponse(HttpStatusCode.OK, outputEvents);
        }

        [Route("api/events/{id}")]
        public HttpResponseMessage GetEvent(long id)
        {
            var evt = _eventRepository.GetById(id);

            var outputEvents = _mappingService.Map<EventWithMatchDto>(evt);

            var filteredMatchEvents = outputEvents.Matches
                                                  .Where(match => match.StartDate > DateTime.Now && match.StartDate < DateTime.Now.AddHours(24))
                                                  .ToSafeReadOnlyCollection();

            outputEvents.Matches = new Collection<MatchBaseDto>(filteredMatchEvents);

            return Request.CreateResponse(HttpStatusCode.OK, outputEvents);
        }
    }
}
