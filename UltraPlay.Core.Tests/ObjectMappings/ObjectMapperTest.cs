﻿using Autofac;
using NUnit.Framework;
using UltraPlay.Core.Tests.ObjectMappings.Models;
using UltraPlay.ObjectMappings;

namespace UltraPlay.Core.Tests.ObjectMappings
{
    [TestFixture]
    public class ObjectMapperTest
    {
        private IContainer _container;

        [SetUp]
        public void SetUp()
        {
            var containerBuilder = new ContainerBuilder();

            containerBuilder.RegisterType<MappingService>()
                            .As<IMappingService>();

            _container = containerBuilder.Build();
        }

        public IMappingService ConfigureMapping()
        {
            var mappingService = _container.Resolve<IMappingService>();

            mappingService.ConfigureMapping<OuterSource, OuterDest>();
            mappingService.ConfigureMapping<InnerSource, InnerDest>();

            return mappingService;
        }

        public IMappingService ConfigureMappingWithChild()
        {
            var mappingService = _container.Resolve<IMappingService>();

            mappingService.ConfigureMapping<OuterSource, OuterDest>();

            return mappingService;
        }

        [Test]
        public void TestMapping()
        {
            var mappingService = ConfigureMapping();

            var source = new OuterSource
            {
                Value = 5,
                Inner = new InnerSource { OtherValue = 15 }
            };

            var dest = mappingService.Map<OuterSource, OuterDest>(source);

            Assert.AreEqual(5, dest.Value);
            Assert.IsNotNull(dest.Inner);
            Assert.AreEqual(15, dest.Inner.OtherValue);
        }

        [Test]
        public void TestMappingWithChild()
        {
            var mappingService = ConfigureMappingWithChild();

            var source = new OuterSource
            {
                Value = 5,
                Inner = new InnerSource { OtherValue = 15 }
            };

            var dest = mappingService.Map<OuterSource, OuterDest>(source);


            Assert.AreEqual(5, dest.Value);
            Assert.IsNotNull(dest.Inner);
            Assert.AreEqual(15, dest.Inner.OtherValue);
        }
    }
}