﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace UltraPlay.TestHelpers
{
    public class XmlSerializationHelper
    {
        public static string SerializeToXmlString(object obj)
        {
            var xmlSerializer = new XmlSerializer(obj.GetType());

            var ns = new XmlSerializerNamespaces();

            ns.Add("", "");
            var output = new StringWriter();
            var streamWriter = XmlWriter.Create(output, new XmlWriterSettings()
            {
                OmitXmlDeclaration = true
            });
            xmlSerializer.Serialize(streamWriter, obj, ns);

            var actualString = output.ToString();
            return actualString;
        }

        public static T Deserialized<T>(string xmlString)
        {
            var xmlSerializer = new XmlSerializer(typeof(T));

            var actualObject = xmlSerializer.Deserialize(new StringReader(xmlString));

            return (T)actualObject;
        }
    }
}
