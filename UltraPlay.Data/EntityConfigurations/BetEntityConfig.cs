﻿using System.Data.Entity.ModelConfiguration;
using UltraPlay.Data.Models;

namespace UltraPlay.Data.EntityConfigurations
{
    public class BetEntityConfig : EntityTypeConfiguration<Bet>
    {
        public BetEntityConfig()
        {
            ToTable("Bets");

            HasKey(bet => bet.Id);

            HasMany(bet => bet.Odds)
                .WithRequired(odd => odd.Bet)
                .WillCascadeOnDelete(true);
        }
    }
}
