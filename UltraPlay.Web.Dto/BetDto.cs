using System.Collections.ObjectModel;

namespace UltraPlay.Web.Dto
{
    public class BetDto
    {
        public string Name { get; set; }
        
        public long Id { get; set; }
        
        public bool IsLive { get; set; }
        
        public virtual Collection<OddDto> Odds { get; set; }
    }
}