using System.Data.Entity.ModelConfiguration;
using UltraPlay.Data.Models;

namespace UltraPlay.Data.EntityConfigurations
{
    public class SportEntityConfig : EntityTypeConfiguration<Sport>
    {
        public SportEntityConfig()
        {
            ToTable("Sports");

            HasKey(sport => sport.Id);

            HasMany(sport => sport.Events)
                .WithRequired(evt => evt.Sport)
                .WillCascadeOnDelete(true);
        }
    }
}