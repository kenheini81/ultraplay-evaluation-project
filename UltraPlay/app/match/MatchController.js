/**
 * Created on 8/7/2015.
 */
import './MatchService';
import './MatchDirective';

class MatchController {
    constructor($scope, $stateParams) {
        this.matchId = $stateParams.matchId;
        console.log(this.matchId);
    }
}

MatchController.$inject = ['$scope', '$stateParams'];

angular.module('ultraplay')
    .controller('MatchController', MatchController);