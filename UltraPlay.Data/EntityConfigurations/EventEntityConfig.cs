using System.Data.Entity.ModelConfiguration;
using UltraPlay.Data.Models;

namespace UltraPlay.Data.EntityConfigurations
{
    public class EventEntityConfig : EntityTypeConfiguration<Event>
    {
        public EventEntityConfig()
        {
            ToTable("Events");

            HasKey(evt => evt.Id);

            HasMany(evt => evt.Matches)
                .WithRequired(match => match.Event)
                .WillCascadeOnDelete(true);
        }
    }
}