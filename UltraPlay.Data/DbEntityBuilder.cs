using System;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration;
using System.Reflection;
using UltraPlay.Data.EntityConfigurations;
using UltraPlay.Data.Models;

namespace UltraPlay.Data
{
    public class DbEntityBuilder
    {
        private DbCompiledModel _dbModel;
        private DbModelBuilder _dbModelBuilder;

        public DbEntityBuilder()
        {
            _dbModelBuilder = new DbModelBuilder(DbModelBuilderVersion.Latest);
        }

        private DbCompiledModel BuildModel()
        {
            _dbModelBuilder = new DbModelBuilder(DbModelBuilderVersion.Latest);

            RegisterModel<Sport, SportEntityConfig>();
            RegisterModel<Match, MatchEntityConfig>();
            RegisterModel<Event, EventEntityConfig>();
            RegisterModel<Bet, BetEntityConfig>();
            RegisterModel<Odd, OddEntityConfig>();


            var connectionString =
                ConfigurationManager.ConnectionStrings[UltraPlayDbContext.DefaultConnectionStringName];


            var dbConnection = new SqlConnectionFactory().CreateConnection(connectionString.ConnectionString);

            return _dbModelBuilder.Build(dbConnection).Compile();
        }

        private void RegisterModel<TEntity>(string tableName = "")
        {
            var entityMethod = typeof(DbModelBuilder).GetMethod("Entity");

            var entTypConfig = entityMethod.MakeGenericMethod(typeof(TEntity))
                .Invoke(_dbModelBuilder, new object[] { });

            if (!string.IsNullOrEmpty(tableName))
                entTypConfig.GetType()
                    .InvokeMember("ToTable",
                        BindingFlags.InvokeMethod,
                        null,
                        entTypConfig,
                        new object[] { tableName });
        }

        private void RegisterModel<TEntity, TEntityConfiguration>(string tableName = "")
        {
            RegisterModel<TEntity>(tableName);
            dynamic instance = Activator.CreateInstance<TEntityConfiguration>();
            if (instance != null && typeof(EntityTypeConfiguration<>).IsInstanceOfType(instance))
            {
                _dbModelBuilder.Configurations.Add(instance);
            }
        }

        public DbCompiledModel GetDbModel()
        {
            if (_dbModel == null)
            {
                lock (this)
                {
                    if (_dbModel == null)
                    {
                        _dbModel = BuildModel();
                    }
                }
            }
            return _dbModel;
        }
    }
}