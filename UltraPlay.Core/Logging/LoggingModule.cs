﻿/**
* Referenced from Orchard CMS Logging Module 
* https://github.com/OrchardCMS/Orchard/blob/dev/src/Orchard/Logging/LoggingModule.cs
*/

using Autofac;
using Autofac.Core;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Hosting;
using Module = Autofac.Module;

namespace UltraPlay.Logging
{
    public class LoggingModule : Module
    {
        private readonly ConcurrentDictionary<string, ILogger> _loggerCache;

        public LoggingModule()
        {
            _loggerCache = new ConcurrentDictionary<string, ILogger>();
            var log4NetPath = HostingEnvironment.MapPath("~/log4net.config");
            log4net.Config.XmlConfigurator.ConfigureAndWatch(new System.IO.FileInfo(log4NetPath));
        }

        protected override void Load(ContainerBuilder moduleBuilder)
        {
            moduleBuilder.Register(CreateLogger).As<ILogger>().InstancePerDependency();
        }

        protected override void AttachToComponentRegistration(IComponentRegistry componentRegistry, IComponentRegistration registration)
        {
            var implementationType = registration.Activator.LimitType;

            // build an array of actions on this type to assign loggers to member properties
            var injectors = BuildLoggerInjectors(implementationType).ToArray();

            // if there are no logger properties, there's no reason to hook the activated event
            if (!injectors.Any())
                return;

            // otherwise, when an instance of this component is activated, inject the loggers on the instance
            registration.Activated += (s, e) =>
                                      {
                                          foreach (var injector in injectors)
                                              injector(e.Context, e.Instance);
                                      };
        }

        /// <summary>
        /// Builds the injector for a specified <see cref="componentType"/>
        /// </summary>
        /// <param name="componentType">The type to get injected</param>
        /// <returns></returns>
        private IEnumerable<Action<IComponentContext, object>> BuildLoggerInjectors(Type componentType)
        {
            var loggerProperties = componentType.GetRuntimeFields()
                                                .Select(p => new
                                                {
                                                    PropertyInfo = p as MemberInfo,
                                                    p.FieldType
                                                })
                                                .Where(x => x.FieldType == typeof(ILogger));

            loggerProperties = loggerProperties.Concat(componentType.GetRuntimeProperties()
                                                                    .Select(p => new
                                                                    {
                                                                        PropertyInfo = p as MemberInfo,
                                                                        FieldType = p.PropertyType
                                                                    })
                                                                    .Where(x => x.FieldType == typeof(ILogger)));

            var actions = new List<Action<IComponentContext, object>>();

            // Return an array of actions that resolve a logger and assign the property
            foreach (var entry in loggerProperties)
            {
                // gets the name of the component type
                var component = componentType.ToString();
                var propertyInfo = entry.PropertyInfo as PropertyInfo;


                if (propertyInfo != null)
                {
                    actions.Add((context, instance) =>
                                {
                                    // try to get the logger instance that already resolved, or resolve a new one based on the component's name
                                    var logger = _loggerCache.GetOrAdd(component, key => context.Resolve<ILogger>(new TypedParameter(typeof(Type), componentType)));
                                    propertyInfo.SetValue(instance, logger);
                                });
                    continue;
                }

                var fieldInfo = entry.PropertyInfo as FieldInfo;
                if (fieldInfo == null)
                    continue;

                actions.Add((context, instance) =>
                            {
                                // try to get the logger instance that already resolved, or resolve a new one based on the component's name
                                var logger = _loggerCache.GetOrAdd(component, key => context.Resolve<ILogger>(new TypedParameter(typeof(Type), componentType)));
                                fieldInfo.SetValue(instance, logger);
                            });
            }
            return actions;
        }

        private static ILogger CreateLogger(IComponentContext context, IEnumerable<Parameter> parameters)
        {
            var enumerable = parameters as Parameter[] ?? parameters.ToArray();
            if (!enumerable.Any())
                return new Logger(typeof(Logger));

            var containingType = enumerable.TypedAs<Type>();

            return new Logger(containingType);
        }
    }
}