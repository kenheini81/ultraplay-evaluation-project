﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace UltraPlay.Data.Repositories
{
    public interface IRepository<TEntity> : IDisposable where TEntity : class
    {
        /// <summary>
        /// Get all elements
        /// </summary>
        /// <remarks>
        ///     Equivalent to SELECT * FROM [entity.table]
        /// </remarks>
        /// <returns></returns>
        IQueryable<TEntity> GetAll();

        /// <summary>
        /// Get element by Identifier
        /// </summary>
        /// <param name="id">Identifier of the entity</param>
        /// <remarks>
        ///     Equivalent to SELECT * FROM [entity.table] WHERE Id='id'
        /// </remarks>
        /// <returns></returns>
        TEntity GetById(params object[] id);

        /// <summary>
        /// Get multiple elements by identifier
        /// </summary>
        /// <param name="ids">List of identifier to get</param>
        /// <returns></returns>
        IQueryable<TEntity> GetByIds(object[] ids);

        /// <summary>
        /// Get the first element that matches expression
        /// </summary>
        /// <param name="predicate">expression for checking</param>
        /// <returns></returns>
        TEntity First(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Get the last element that matches expression
        /// </summary>
        /// <param name="predicate">expression for checking</param>
        /// <returns></returns>
        TEntity Last(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Insert new entity
        /// </summary>
        /// <param name="entity">Entity to insert</param>
        /// <returns></returns>
        void Insert(TEntity entity);

        /// <summary>
        /// Inserts of updates an entity using the <see cref="identifierExpression"/> to determine which to update or insert
        /// </summary>
        /// <param name="identifierExpression">The <see cref="Expression"/> indicates which property of the <see cref="entity"/> is identifier for update or insert</param>
        /// <param name="entity">The entity to insert or update</param>
        void InsertOrUpdate(Expression<Func<TEntity, object>> identifierExpression, TEntity entity);

        /// <summary>
        /// Inserts of updates many entities using the <see cref="identifierExpression"/> to determine which to update or insert
        /// </summary>
        /// <param name="identifierExpression">The <see cref="Expression"/> indicates which property of the <see cref="entities"/> is identifier for update or insert</param>
        /// <param name="entities"></param>
        void InsertOrUpdate(Expression<Func<TEntity, object>> identifierExpression, IEnumerable<TEntity> entities);

        /// <summary>
        /// Update an element
        /// </summary>
        /// <param name="entity">Element to update</param>
        /// <returns></returns>
        void Update(TEntity entity);

        /// <summary>
        /// Update every entity satisfies <see cref="predicate"/> by a specific <see cref="dispatch"/>.
        /// </summary>
        /// <remarks>
        ///     Equivalent to Update [entity.table] set [dispatch.values.here] where [predicate.condition]
        /// </remarks>
        /// <param name="predicate">Condition to fetch the entities</param>
        /// <param name="dispatch">Represents the method of setting entities' values</param>
        void Update(Expression<Func<TEntity, bool>> predicate, Action<TEntity> dispatch);

        /// <summary>
        /// Delete element
        /// </summary>
        /// <param name="entity">Entity to delete</param>
        void Delete(TEntity entity);

        /// <summary>
        /// Delete all entities that satisfy <see cref="predicate"/>
        /// </summary>
        /// <remarks>
        ///     Equivalent to DELETE FROM [entity.table] where [predicate.condition]
        /// </remarks>
        /// <param name="predicate">Condition to fetch the entities</param>
        void DeleteByCondition(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Count amount of elements that matches the <see cref="expression"/>
        /// </summary>
        /// <param name="expression">The condition expression</param>
        /// <remarks>
        ///     Equivalent to SELECT COUNT (*) FROM [entity.Table] WHERE [expression]
        /// </remarks>
        /// <returns></returns>
        int Count(Expression<Func<TEntity, bool>> expression = null);

        /// <summary>
        /// Indicates if there is any element that matches the <see cref="expression"/>
        /// </summary>
        /// <param name="expression">The condition expression</param>
        /// <returns></returns>
        bool Any(Expression<Func<TEntity, bool>> expression);

        /// <summary>
        /// Fetch list of elements match expression
        /// </summary>
        /// <param name="expression">expression for checking</param>
        /// <returns></returns>
        IQueryable<TEntity> Fetch(Expression<Func<TEntity, bool>> expression);

        int SaveChanges();
    }
}
