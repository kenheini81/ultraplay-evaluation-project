﻿using System.Data.Entity.ModelConfiguration;
using UltraPlay.Data.Models;

namespace UltraPlay.Data.EntityConfigurations
{
    public class OddEntityConfig : EntityTypeConfiguration<Odd>
    {
        public OddEntityConfig()
        {
            ToTable("Odds");

            HasKey(odd => odd.Id);
        }
    }
}