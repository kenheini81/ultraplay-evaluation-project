﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace UltraPlay.Data
{
    public class UltraPlayMigrationConfig : MigrationsConfigBase<UltraPlayDbContext> { }

    public class UltraPlayDbConfiguration : DbConfiguration
    {
        public UltraPlayDbConfiguration()
        {
            SetDefaultConnectionFactory(new SqlConnectionFactory());
            SetDatabaseInitializer(new MigrateDatabaseToLatestVersion<UltraPlayDbContext, UltraPlayMigrationConfig>());
        }
    }
}