﻿namespace UltraPlay.Web.Dto
{
    public class OddDto
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public double Value { get; set; }
    }
}