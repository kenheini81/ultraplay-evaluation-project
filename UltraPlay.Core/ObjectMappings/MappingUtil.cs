﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AutoMapper;

namespace UltraPlay.ObjectMappings
{
    public class MappingUtil
    {
        public static IMappingExpression<TSource, TDestination> CreateNestedMappers<TSource, TDestination>()
        {
            var sourceType = typeof(TSource);
            var destinationType = typeof(TDestination);

            var sourceProperties = sourceType.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            var destinationProperties = destinationType.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var destinationProperty in destinationProperties)
            {
                var destinationPropertyType = destinationProperty.PropertyType;
                if (Filter(destinationPropertyType))
                    continue;

                var sourceProperty = sourceProperties.FirstOrDefault(prop => NameMatches(prop.Name, destinationProperty.Name));
                if (sourceProperty == null)
                    continue;

                var sourcePropertyType = sourceProperty.PropertyType;
                if (destinationPropertyType.IsGenericType)
                {
                    var destinationGenericType = destinationPropertyType.GetGenericArguments()[0];
                    if (Filter(destinationGenericType))
                        continue;

                    var sourceGenericType = sourcePropertyType.GetGenericArguments()[0];
                    Mapper.CreateMap(sourceGenericType, destinationGenericType);
                }
                else
                {
                    Mapper.CreateMap(sourcePropertyType, destinationPropertyType);
                }
            }

            return Mapper.CreateMap<TSource, TDestination>();
        }

        /// <summary>
        /// 过滤
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static bool Filter(Type type)
        {
            return type.IsPrimitive || NoPrimitiveTypes.Contains(type.Name);
        }

        private static readonly HashSet<string> NoPrimitiveTypes = new HashSet<string> { "String", "DateTime", "Decimal" };

        private static bool NameMatches(string memberName, string nameToMatch)
        {
            return String.Compare(memberName, nameToMatch, StringComparison.OrdinalIgnoreCase) == 0;
        }
    }
}