/**
 * Created on 8/6/2015.
 */
var apiEndpoint = '/api';
export default {
    intervalPolling: 10,
    apis: {
        getAllSports: apiEndpoint + "/sports",
        getSport: apiEndpoint + "/sports/",
        getEvent: apiEndpoint + "/events/",
        getMatch: apiEndpoint + "/matches/",
        getOdd: apiEndpoint + "/odds/"
    }
};