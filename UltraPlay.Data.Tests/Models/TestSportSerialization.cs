﻿using NUnit.Framework;
using System;
using System.Collections.ObjectModel;
using UltraPlay.Data.Models;
using XmlSerializationHelper = UltraPlay.TestHelpers.XmlSerializationHelper;

namespace UltraPlay.Data.Tests.Models
{
    [TestFixture]
    public class TestSportSerialization
    {
        const string xmlString =
            "<Sport Name=\"SportName\" ID=\"1\">" +
            "<Event Name=\"EventName-1\" ID=\"1\" CategoryID=\"1\" IsLive=\"false\">" +
            "<Match Name=\"MatchName-1-1\" ID=\"1\" StartDate=\"2015-08-02T07:00:00\" MatchType=\"PreMatch\" />" +
            "<Match Name=\"MatchName-1-2\" ID=\"2\" StartDate=\"2015-08-02T07:00:00\" MatchType=\"PreMatch\" />" +
            "</Event>" +
            "<Event Name=\"EventName-2\" ID=\"2\" CategoryID=\"2\" IsLive=\"true\">" +
            "<Match Name=\"MatchName-2-1\" ID=\"3\" StartDate=\"2015-08-02T07:00:00\" MatchType=\"PreMatch\" />" +
            "<Match Name=\"MatchName-2-2\" ID=\"4\" StartDate=\"2015-08-02T07:00:00\" MatchType=\"PreMatch\" />" +
            "</Event>" +
            "</Sport>";

        [Test(Description = "Deserialized given XML to modle should be proceeded properly")]
        public void SportModel_ShouldBeSerializedToXml()
        {
            var _event1 = new Event
            {
                Name = "EventName-1",
                Id = 1,
                CategoryId = 1,
                IsLive = false,
                Matches = new Collection<Match>
                {
                    new Match
                    {
                        Name = "MatchName-1-1",
                        Id=1,
                        StartDate = new DateTime(2015,8,2,7,0,0),
                        MatchType = "PreMatch"
                    },
                    new Match
                    {
                        Name = "MatchName-1-2",
                        Id=2,
                        StartDate = new DateTime(2015,8,2,7,0,0),
                        MatchType = "PreMatch"
                    }
                }
            };
            var _event2 = new Event
            {
                Name = "EventName-2",
                Id = 2,
                CategoryId = 2,
                IsLive = true,
                Matches = new Collection<Match>
                {
                    new Match
                    {
                        Name = "MatchName-2-1",
                        Id=3,
                        StartDate = new DateTime(2015,8,2,7,0,0),
                        MatchType = "PreMatch"
                    },
                    new Match
                    {
                        Name = "MatchName-2-2",
                        Id=4,
                        StartDate = new DateTime(2015,8,2,7,0,0),
                        MatchType = "PreMatch"
                    }
                }
            };

            var sport = new Sport
            {
                Name = "SportName",
                Events = new Collection<Event>
                {
                    _event1,
                    _event2
                },
                Id = 1
            };

            var actualString = XmlSerializationHelper.SerializeToXmlString(sport);
            Console.WriteLine("Serialized object: " + actualString);
            Assert.AreEqual(xmlString, actualString);
        }

        [Test(Description = "Deserialized given XML to modle should be proceeded properly")]
        public void SportModel_ShouldBeDeserializedFromXml()
        {
            var actualObject = XmlSerializationHelper.Deserialized<Sport>(xmlString);

            Assert.AreEqual("SportName", actualObject.Name);
            Assert.AreEqual(1, actualObject.Id);
            Assert.AreEqual("EventName-1", actualObject.Events[0].Name);
            Assert.AreEqual("EventName-2", actualObject.Events[1].Name);
            Assert.AreEqual("MatchName-1-1", actualObject.Events[0].Matches[0].Name);
            Assert.AreEqual("MatchName-1-2", actualObject.Events[0].Matches[1].Name);
            Assert.AreEqual("MatchName-2-1", actualObject.Events[1].Matches[0].Name);
            Assert.AreEqual("MatchName-2-2", actualObject.Events[1].Matches[1].Name);
        }
    }
}
