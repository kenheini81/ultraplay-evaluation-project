﻿var gulp = require("gulp");
var fs = require("fs");
var browserify = require("browserify");
var watchify = require("watchify");
var babelify = require("babelify");
var rimraf = require("rimraf");
var less = require("gulp-less");
var source = require("vinyl-source-stream");
var _ = require("lodash");
var browserSync = require("browser-sync");
var path = require('path');
var reload = browserSync.reload;

var config = {
    entryFile: "./app/main.js",
    outputDir: "./dist/",
    outputFile: "app.js"
};

// clean the output directory
gulp.task("clean", function (cb) {
    rimraf(config.outputDir, cb);
});

var bundler;

function getBundler() {
    if (!bundler) {
        bundler = watchify(browserify(config.entryFile, _.extend({debug: true}, watchify.args)));
    }
    return bundler;
}

function bundle() {
    return getBundler()
        .transform(babelify)
        .bundle()
        .on("error", function (err) {
            console.log("Error: " + err.message);
        })
        .pipe(source(config.outputFile))
        .pipe(gulp.dest(config.outputDir))
        .pipe(reload({stream: true}));
}

gulp.task("build-persistent", [], function () {
    return bundle();
});

gulp.task("build", ["build-persistent"], function () {
    gulp.start("less");
});

gulp.task("watch", ["build-persistent"], function () {

    browserSync({
        server: {
            baseDir: "./"
        }
    });

    getBundler().on("update", function () {
        gulp.start("build-persistent");
    });
});

gulp.task('less', function () {
    gulp.src('./assets/**/*.less')
        .pipe(less({
            paths: [path.join(__dirname, 'less', 'includes')]
        }))
        .pipe(gulp.dest(config.outputDir));
});

// WEB SERVER
gulp.task("serve", function () {
    browserSync({
        server: {
            baseDir: "./"
        }
    });
});