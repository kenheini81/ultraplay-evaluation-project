﻿using System;
using System.Collections.Generic;
using UltraPlay.Contexts;

namespace UltraPlay.BackgroundTask
{
    public interface IBackgroundTaskRunner : IDependency
    {
        void DoWork();
    }

    public class BackgroundTaskRunner : Component, IBackgroundTaskRunner
    {
        private readonly IEnumerable<IBackgroundTask> _tasks;
        private readonly UltraPlayBackgroundContext _backgroundContext;

        public BackgroundTaskRunner(IEnumerable<IBackgroundTask> tasks, UltraPlayBackgroundContext backgroundContext)
        {
            _tasks = tasks;
            _backgroundContext = backgroundContext;
        }

        public void DoWork()
        {
            foreach (var task in _tasks)
            {
                ExecuteTask(task);
            }
        }

        private void ExecuteTask(IBackgroundTask taskHandler)
        {
            using (_backgroundContext.SimulateWebRequestScope())
            {
                try
                {
                    taskHandler.Execute();
                }
                catch (Exception e)
                {
                    Logger.Error(e, "Error while processing background task");
                }
            }
        }
    }
}