﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UltraPlay.Data.Models;
using UltraPlay.Data.Repositories;
using UltraPlay.ObjectMappings;
using UltraPlay.Web.Dto;

namespace UltraPlay.Web.Controllers
{
    public class SportsController : ApiController
    {
        private readonly IRepository<Sport> _sportRepository;
        private readonly IMappingService _mappingService;

        public SportsController(IRepository<Sport> sportRepository, IMappingService mappingService)
        {
            _sportRepository = sportRepository;
            _mappingService = mappingService;
        }

        [Route("api/sports")]
        public HttpResponseMessage GetAllSports()
        {
            var sports = _sportRepository.GetAll();

            var outputSports = _mappingService.Map<IEnumerable<SportDto>>(sports);

            return Request.CreateResponse(HttpStatusCode.OK, outputSports);
        }

        [Route("api/sports/{id:long}")]
        public IHttpActionResult GetSport(long id)
        {
            var sports = _sportRepository.GetById(id);

            if (sports == null) return NotFound();

            var outputSports = _mappingService.Map<SportWithEventDto>(sports);

            return Ok(outputSports);
        }
    }
}
