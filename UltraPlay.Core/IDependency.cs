﻿using Newtonsoft.Json;
using UltraPlay.Logging;

namespace UltraPlay
{
    public interface IDependency
    {
    }

    public interface ISingletonDependency : IDependency
    {
    }

    public interface IUnitOfWorkDependency : IDependency
    {
    }

    public abstract class Component : IDependency
    {
        protected Component()
        {
            Logger = NullLogger.Instance;
        }

        /// <summary>
        /// Logger service
        /// </summary>
        [JsonIgnore]
        public ILogger Logger { get; set; }
    }
}
