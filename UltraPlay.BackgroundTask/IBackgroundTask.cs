﻿namespace UltraPlay.BackgroundTask
{
    public interface IBackgroundTask : IDependency
    {
        void Execute();
    }
}