﻿using System.Collections.ObjectModel;

namespace UltraPlay.Web.Dto
{
    public class EventBaseDto
    {
        public long Id { get; set; }

        public string Name { get; set; }
        public long CategoryId { get; set; }
        public bool IsLive { get; set; }
    }

    public class EventWithMatchDto : EventBaseDto
    {
        public virtual Collection<MatchBaseDto> Matches { get; set; }
    }
}