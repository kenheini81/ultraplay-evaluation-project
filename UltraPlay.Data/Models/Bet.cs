using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace UltraPlay.Data.Models
{
    [DataContract]
    public class Bet
    {
        [XmlAttribute(AttributeName = "Name"), JsonProperty]
        public string Name { get; set; }

        [XmlAttribute(AttributeName = "ID"), JsonProperty]
        [DatabaseGenerated(DatabaseGeneratedOption.None), Key]
        public long Id { get; set; }

        [XmlAttribute(AttributeName = "IsLive"), JsonProperty]
        public bool IsLive { get; set; }

        [XmlElement("Odd"), JsonProperty]
        public virtual Collection<Odd> Odds { get; set; }

        [XmlIgnore]
        public virtual Match Match { get; set; }
    }
}