﻿using System;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace UltraPlay.Data.Models
{
    [DataContract, XmlRoot(ElementName = "XmlSports")]
    public class XmlSport
    {
        [XmlAttribute(AttributeName = "CreateDate")]
        public DateTime CreateDate { get; set; }

        [XmlElement("Sport")]
        public virtual Collection<Sport> Sports { get; set; }
    }
}