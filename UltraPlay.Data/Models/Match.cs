﻿using Newtonsoft.Json;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace UltraPlay.Data.Models
{
    [DataContract]
    public class Match
    {
        [XmlAttribute(AttributeName = "Name"), JsonProperty]
        public string Name { get; set; }

        [XmlAttribute(AttributeName = "ID"), JsonProperty]
        [DatabaseGenerated(DatabaseGeneratedOption.None), Key]
        public long Id { get; set; }

        [XmlAttribute(AttributeName = "StartDate"), JsonProperty]
        public DateTime StartDate { get; set; }

        [XmlAttribute(AttributeName = "MatchType"), JsonProperty]
        public string MatchType { get; set; }

        [XmlElement("Bet"), JsonProperty]
        public virtual Collection<Bet> Bets { get; set; }

        [XmlIgnore]
        public virtual Event Event { get; set; }
    }
}
