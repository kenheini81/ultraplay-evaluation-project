﻿using System.Data.Entity;
using System.Data.Entity.Migrations;

namespace UltraPlay.Data
{
    public class MigrationsConfigBase<TDbContext> : DbMigrationsConfiguration<TDbContext>
        where TDbContext : DbContext
    {
        public MigrationsConfigBase()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = typeof(TDbContext).FullName;
            MigrationsNamespace = typeof(TDbContext).Namespace + ".Migrations";
            var migrationDirectory =
                MigrationsNamespace.Replace(typeof(TDbContext).Assembly.GetName().Name, "")
                                   .Trim('.')
                                   .Replace('.', '\\');
            MigrationsDirectory = migrationDirectory;
            MigrationsAssembly = typeof(TDbContext).Assembly;
        }
    }
}