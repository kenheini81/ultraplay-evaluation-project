using System.Net;
using System.Net.Http;
using System.Web.Http;
using UltraPlay.Data.Models;
using UltraPlay.Data.Repositories;
using UltraPlay.ObjectMappings;
using UltraPlay.Web.Dto;

namespace UltraPlay.Web.Controllers
{
    public class OddController : ApiController
    {
        private readonly IRepository<Odd> _oddRepository;
        private readonly IMappingService _mappingService;

        public OddController(IRepository<Odd> oddRepository, IMappingService mappingService)
        {
            _oddRepository = oddRepository;
            _mappingService = mappingService;
        }

        [Route("api/odds/{id:long}")]
        public HttpResponseMessage GetOdd(long id)
        {
            var odd = _oddRepository.GetById(id);

            var outputSports = _mappingService.Map<OddDto>(odd);

            return Request.CreateResponse(HttpStatusCode.OK, outputSports);
        }
    }
}