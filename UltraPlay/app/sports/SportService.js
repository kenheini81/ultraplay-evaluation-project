/**
 * Created on 8/6/2015.
 */
class SportService {
    constructor($http, $config) {
        this.ajaxService = $http;
        this.config = $config;
    }

    getSports(callback) {
        var _this = this;
        var url = _this.config.apis.getAllSports;

        _this.ajaxService({
            url: url,
            type: 'get',
        })
            .success(callback)
            .then(function (error) {

            });
    }

    getSport(sportId, callback) {
        var _this = this;
        var url = _this.config.apis.getSport + sportId;

        _this.ajaxService({
            url: url,
            type: 'get'
        })
            .success(callback)
            .then(function (error) {

            });
    }
}

SportService.$inject = ['$http', '$config'];

angular.module('ultraplay')
    .service('SportService', SportService);