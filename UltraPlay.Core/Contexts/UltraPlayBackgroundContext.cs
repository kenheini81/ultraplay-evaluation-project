﻿using Autofac;
using Autofac.Core.Lifetime;

namespace UltraPlay.Contexts
{
    public class UltraPlayBackgroundContext
    {
        public ILifetimeScope LifetimeScope { get; set; }

        public ILifetimeScope SimulateWebRequestScope()
        {
            return LifetimeScope.BeginLifetimeScope(MatchingScopeLifetimeTags.RequestLifetimeScopeTag);
        }
    }
}