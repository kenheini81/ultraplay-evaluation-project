﻿using AutoMapper;

namespace UltraPlay.ObjectMappings
{
    public interface IMappingService : ISingletonDependency
    {
        IMappingExpression<TSource, TDestination> ConfigureMapping<TSource, TDestination>();

        TOut Map<TIn, TOut>(TIn model);

        TOut Map<TOut>(object model);
    }

    public class MappingService : IMappingService
    {
        public IMappingExpression<TSource, TDestination> ConfigureMapping<TSource, TDestination>()
        {
            return MappingUtil.CreateNestedMappers<TSource, TDestination>();
        }

        public TOut Map<TIn, TOut>(TIn model)
        {
            return Mapper.Map<TIn, TOut>(model);
        }

        public TOut Map<TOut>(object model)
        {
            return Mapper.Map<TOut>(model);
        }
    }
}