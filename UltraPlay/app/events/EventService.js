/**
 * Created on 8/6/2015.
 */
class EventService {
    constructor($http, $config) {
        this.ajaxService = $http;
        this.config = $config;
    }

    getEvent(eventId, callback) {
        var _this = this;
        var url = _this.config.apis.getEvent + eventId;

        _this.ajaxService({
            url: url,
            type: 'get'
        })
            .success(callback)
            .then(function (error) {

            });
    }
}

EventService.$inject = ['$http', '$config'];

angular.module('ultraplay')
    .service('EventService', EventService);