﻿using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using System.Collections.Generic;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using UltraPlay.BackgroundTask;
using UltraPlay.Data;
using UltraPlay.Web.Dto;

namespace UltraPlay.Web
{
    public class WebApiApplication : HttpApplication
    {
        private readonly List<Assembly> _dependencies;
        private IContainer _applicationContainer;
        private IEnumerable<IApplicationEventHandler> _eventHandlers;

        public WebApiApplication()
        {
            _dependencies = new List<Assembly>
                            {
                                typeof (WebApiApplication).Assembly,
                                typeof (IDependency).Assembly,
                                typeof (UltraPlayDataModule).Assembly,
                                typeof (IBackgroundClock).Assembly,
                                typeof (ApiProxy.ApiProxy).Assembly,
                                typeof(UltraPlayDtoModule).Assembly
                            };
        }

        protected void Application_Start()
        {
            _applicationContainer = AutofacContainerConfigure.BuildContainer(_dependencies);

            DependencyResolver.SetResolver(new AutofacDependencyResolver(_applicationContainer));

            _eventHandlers = _applicationContainer.Resolve<IEnumerable<IApplicationEventHandler>>();
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(configuration =>
            {
                WebApiConfig.Register(configuration);
                configuration.DependencyResolver = new AutofacWebApiDependencyResolver(_applicationContainer);
            });

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            foreach (var eventHandler in _eventHandlers)
            {
                eventHandler.OnApplicationStarted();
            }
        }
    }
}
