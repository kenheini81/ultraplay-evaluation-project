﻿using Moq;
using NUnit.Framework;
using System;

namespace UltraPlay.ApiProxy.Tests
{
    [TestFixture]
    public class ApiProxyTest
    {
        private ApiProxy _sut;
        private Mock<IApiProxyHandler> _apiProxyHandler;

        [SetUp]
        public void SetUp()
        {
            _apiProxyHandler = new Mock<IApiProxyHandler>();
            _sut = new ApiProxy(_apiProxyHandler.Object);
        }

        [Test]
        public async void TestGetDataAsync_ShouldCallOnDownloadedCallBackAfterGotData()
        {
            var output = await _sut.GetDataAsync();
            Console.WriteLine(output);
            _apiProxyHandler.Verify(x => x.OnDataDownloaded(It.IsAny<string>()));
        }


        [Test]
        public void TestGetData_ShouldCallOnDownloadedCallBackAfterGotData()
        {
            _sut.GetData();
            _apiProxyHandler.Verify(x => x.OnDataDownloaded(It.IsAny<string>()));
        }
    }
}
