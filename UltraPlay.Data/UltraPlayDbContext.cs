﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace UltraPlay.Data
{
    public interface IDbContext
    {
        DbSet<T> Set<T>() where T : class;

        int SaveChanges();
        DbEntityEntry Entry(object entity);
        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;
    }


    public class UltraPlayDbContext : DbContext, IDbContext
    {
        public const string DefaultConnectionStringName = "DefaultConnection";

        public UltraPlayDbContext()
            : base(DefaultConnectionStringName, new DbEntityBuilder().GetDbModel())
        {

        }

        public UltraPlayDbContext(DbEntityBuilder dbEntityBuilder)
            : base(DefaultConnectionStringName, dbEntityBuilder.GetDbModel())
        {

        }
    }
}