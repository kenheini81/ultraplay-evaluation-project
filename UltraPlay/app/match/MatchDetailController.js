/**
 * Created on 8/7/2015.
 */
import './MatchService';

class MatchController {
    constructor($scope, MatchService, $interval, config) {
        this.MatchService = MatchService;
        this.$interval = $interval;
        this.data = {};
        this.$scope = $scope;
        var self = this;
        this.matchId = null;

        $scope.$watch('matchId', function (oldVal, newVal) {
            self.matchId = newVal;
            self.refreshMatchData();
        });
        $interval(this.refreshMatchData.bind(this), config.intervalPolling * 1000);
    }

    refreshMatchData() {
        console.log('matchId: ', this.matchId);
        var matchId = this.matchId;
        if (!matchId) return;
        this.MatchService.getMatchData(matchId, this.onRefreshMatchDataComplete.bind(this));
    }

    onRefreshMatchDataComplete(data) {
        // mark values that increase or decrease
        _.each(this.data.Bets, function (bet) {
            _.each(bet.Odds, function (odd) {
                odd.OldValue = odd.Value;
            });
        });

        this.data = _.merge(this.data, data);
    }
}

MatchController.$inject = ['$scope', 'MatchService', '$interval', '$config'];

angular.module('ultraplay')
    .controller('MatchDetailController', MatchController);