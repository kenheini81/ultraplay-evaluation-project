using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UltraPlay.Data.Models;
using UltraPlay.Data.Repositories;
using UltraPlay.ObjectMappings;
using UltraPlay.Web.Dto;

namespace UltraPlay.Web.Controllers
{
    public class MatchController : ApiController
    {
        private readonly IRepository<Match> _matchRepository;
        private readonly IMappingService _mappingService;

        public MatchController(IRepository<Match> matchRepository, IMappingService mappingService)
        {
            _matchRepository = matchRepository;
            _mappingService = mappingService;
        }

        [Route("api/matches")]
        public HttpResponseMessage GetMatches()
        {
            var matches = _matchRepository.GetAll();

            var outputSports = _mappingService.Map<IEnumerable<MatchBaseDto>>(matches);

            return Request.CreateResponse(HttpStatusCode.OK, outputSports);
        }

        [Route("api/matches/{id:long}")]
        public HttpResponseMessage GetMatch(long id)
        {
            var match = _matchRepository.GetById(id);

            var outputSports = _mappingService.Map<MatchWithBetDto>(match);

            return Request.CreateResponse(HttpStatusCode.OK, outputSports);
        }
    }
}