﻿using System;

namespace UltraPlay.Logging
{
    public interface ILogger
    {
        void Info(string message);

        void Information(string message);

        void Warn(Exception exception);
        void Warn(string message);
        void Warn(Exception exception, string message, params object[] format);

        void Debug(string message);

        void Error(Exception exception);
        void Error(Exception exception, string message, params object[] format);
        void Error(string message);

        void Fatal(Exception exception);
        void Fatal(string message, Exception exception);
        void Fatal(string message);
    }
}