﻿using System.Web.Mvc;

namespace UltraPlay.Web.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            return File(Url.Content("~/index.html"), "text/html");
        }
    }
}
