﻿using UltraPlay.Data.Models;
using UltraPlay.ObjectMappings;
using UltraPlay.Web.Dto;

namespace UltraPlay.Web.Services
{
    public class UltraPlayObjectMappingRegistration : IObjectMappingRegistration
    {
        public void ConfigureMapping(IMappingService map)
        {
            map.ConfigureMapping<Bet, BetDto>();
            map.ConfigureMapping<Odd, OddDto>();
            map.ConfigureMapping<Match, MatchBaseDto>();
            map.ConfigureMapping<Match, MatchWithBetDto>();
            map.ConfigureMapping<Event, EventBaseDto>();
            map.ConfigureMapping<Event, EventWithMatchDto>();
            map.ConfigureMapping<Sport, SportDto>();
            map.ConfigureMapping<Sport, SportWithEventDto>();
        }
    }
}