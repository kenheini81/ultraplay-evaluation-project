namespace UltraPlay.BackgroundTask
{
    public class BackgroundTaskApplicationEventHandler : IApplicationEventHandler
    {
        private readonly IBackgroundClock _backgroundClock;

        public BackgroundTaskApplicationEventHandler(IBackgroundClock backgroundClock)
        {
            _backgroundClock = backgroundClock;
        }

        public void OnApplicationStarted()
        {
            _backgroundClock.Activate();
        }
    }
}