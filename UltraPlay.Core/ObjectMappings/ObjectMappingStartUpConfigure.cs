using System.Collections.Generic;

namespace UltraPlay.ObjectMappings
{
    public class ObjectMappingStartUpConfigure : IApplicationEventHandler
    {
        private readonly IMappingService _mappingService;
        private readonly IEnumerable<IObjectMappingRegistration> _objectMappingRegistrations;

        public ObjectMappingStartUpConfigure(IMappingService mappingService, IEnumerable<IObjectMappingRegistration> objectMappingRegistrations)
        {
            _mappingService = mappingService;
            _objectMappingRegistrations = objectMappingRegistrations;
        }

        public void OnApplicationStarted()
        {
            foreach (var register in _objectMappingRegistrations)
            {
                register.ConfigureMapping(_mappingService);
            }
        }
    }
}