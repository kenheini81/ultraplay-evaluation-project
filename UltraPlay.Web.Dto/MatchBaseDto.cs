﻿using System;
using System.Collections.ObjectModel;

namespace UltraPlay.Web.Dto
{
    public class MatchBaseDto
    {
        public string Name { get; set; }

        public long Id { get; set; }

        public DateTime StartDate { get; set; }

        public string MatchType { get; set; }
    }

    public class MatchWithBetDto : MatchBaseDto
    {
        public virtual Collection<BetDto> Bets { get; set; }
    }
}