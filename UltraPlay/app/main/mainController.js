﻿class mainController {
    constructor(SportService) {
        this.data = [];
        this.SportService = SportService;
        this.refreshData();
    }

    refreshData() {
        this.SportService.getSports(this.refreshSportData.bind(this));
    }

    refreshSportData(data) {
        this.data = data;
    }
}

mainController.$inject = ['SportService'];

angular.module('ultraplay')
    .controller('mainController', mainController);