﻿using NUnit.Framework;
using System;
using System.Collections.ObjectModel;
using UltraPlay.Data.Models;
using XmlSerializationHelper = UltraPlay.TestHelpers.XmlSerializationHelper;

namespace UltraPlay.Data.Tests.Models
{
    [TestFixture]
    public class TestEventSerialization
    {
        const string xmlString =
            "<Event Name=\"EventName\" ID=\"1\" CategoryID=\"2\" IsLive=\"false\">" +
            "<Match Name=\"MatchName-1\" ID=\"1\" StartDate=\"2015-08-02T07:00:00\" MatchType=\"PreMatch\" />" +
            "<Match Name=\"MatchName-2\" ID=\"2\" StartDate=\"2015-08-02T07:00:00\" MatchType=\"PreMatch\" />" +
            "</Event>";

        [Test(Description = "Serialize model object to XML should be proceeded properly")]
        public void EventModel_ShouldBeSerializedToXml()
        {

            var _event = new Event
            {
                Name = "EventName",
                Id = 1,
                CategoryId = 2,
                IsLive = false,
                Matches = new Collection<Match>
                {
                    new Match
                    {
                        Name = "MatchName-1",
                        Id=1,
                        StartDate = new DateTime(2015,8,2,7,0,0),
                        MatchType = "PreMatch"
                    },
                    new Match
                    {
                        Name = "MatchName-2",
                        Id=2,
                        StartDate = new DateTime(2015,8,2,7,0,0),
                        MatchType = "PreMatch"
                    }
                }
            };

            var actualString = XmlSerializationHelper.SerializeToXmlString(_event);
            Console.WriteLine("Serialized object: " + actualString);
            Assert.AreEqual(xmlString, actualString);
        }

        [Test(Description = "Deserialized given XML to modle should be proceeded properly")]
        public void EventModel_ShouldBeDeserializedFromXml()
        {
            var actualObject = XmlSerializationHelper.Deserialized<Event>(xmlString);

            Assert.AreEqual("EventName", actualObject.Name);
            Assert.AreEqual(1, actualObject.Id);
            Assert.AreEqual(2, actualObject.CategoryId);
            Assert.AreEqual("MatchName-1", actualObject.Matches[0].Name);
            Assert.AreEqual("MatchName-2", actualObject.Matches[1].Name);
        }
    }
}
