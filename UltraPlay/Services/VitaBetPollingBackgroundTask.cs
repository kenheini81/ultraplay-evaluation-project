﻿using System;
using UltraPlay.ApiProxy;
using UltraPlay.BackgroundTask;

namespace UltraPlay.Web.Services
{
    public class VitaBetPollingBackgroundTask : Component, IBackgroundTask
    {
        private readonly IApiProxy _apiProxy;

        public VitaBetPollingBackgroundTask(IApiProxy apiProxy)
        {
            _apiProxy = apiProxy;
        }

        public void Execute()
        {
            Logger.Info("Background task starting: " + DateTimeOffset.Now.ToString("R"));
            _apiProxy.GetData();
            Logger.Info("Polling Vitalbet data finished: " + DateTimeOffset.Now.ToString("R"));
        }
    }
}