﻿using NUnit.Framework;
using UltraPlay.Data.Models;
using XmlSerializationHelper = UltraPlay.TestHelpers.XmlSerializationHelper;

namespace UltraPlay.Data.Tests.Models
{
    [TestFixture]
    public class TestXmlSportSerialization
    {
        private const string xmlString =
            "<XmlSports CreateDate=\"2015-08-03T06:28:15\">" +
            "<Sport Name=\"SportName\" ID=\"1\">" +
            "<Event Name=\"EventName-1\" ID=\"1\" CategoryID=\"1\" IsLive=\"false\">" +
            "<Match Name=\"MatchName-1-1\" ID=\"1\" StartDate=\"2015-08-02T07:00:00\" MatchType=\"PreMatch\" />" +
            "<Match Name=\"MatchName-1-2\" ID=\"2\" StartDate=\"2015-08-02T07:00:00\" MatchType=\"PreMatch\" />" +
            "</Event>" +
            "<Event Name=\"EventName-2\" ID=\"2\" CategoryID=\"2\" IsLive=\"false\">" +
            "<Match Name=\"MatchName-2-1\" ID=\"3\" StartDate=\"2015-08-02T07:00:00\" MatchType=\"PreMatch\" />" +
            "<Match Name=\"MatchName-2-2\" ID=\"4\" StartDate=\"2015-08-02T07:00:00\" MatchType=\"PreMatch\" />" +
            "</Event>" +
            "</Sport>" +
            "<Sport Name=\"SportName-2\" ID=\"2\">" +
            "<Event Name=\"EventName-3\" ID=\"3\" CategoryID=\"1\" IsLive=\"false\">" +
            "<Match Name=\"MatchName-3-1\" ID=\"5\" StartDate=\"2015-08-02T07:00:00\" MatchType=\"PreMatch\" />" +
            "<Match Name=\"MatchName-3-2\" ID=\"6\" StartDate=\"2015-08-02T07:00:00\" MatchType=\"PreMatch\" />" +
            "</Event>" +
            "<Event Name=\"EventName-4\" ID=\"4\" CategoryID=\"2\" IsLive=\"false\">" +
            "<Match Name=\"MatchName-4-1\" ID=\"7\" StartDate=\"2015-08-02T07:00:00\" MatchType=\"PreMatch\" />" +
            "<Match Name=\"MatchName-4-2\" ID=\"8\" StartDate=\"2015-08-02T07:00:00\" MatchType=\"PreMatch\" />" +
            "</Event>" +
            "</Sport>" +
            "</XmlSports>";

        [Test(Description = "Deserialized given XML to modle should be proceeded properly")]
        public void XmlSportModel_ShouldBeDeserializedFromXml()
        {
            var actualObject = XmlSerializationHelper.Deserialized<XmlSport>(xmlString);

            Assert.AreEqual(3, actualObject.CreateDate.Day);
            Assert.AreEqual(8, actualObject.CreateDate.Month);
            Assert.AreEqual(2015, actualObject.CreateDate.Year);

            Assert.AreEqual(2, actualObject.Sports.Count);
            Assert.AreEqual("SportName", actualObject.Sports[0].Name);
            Assert.AreEqual("SportName-2", actualObject.Sports[1].Name);
        }
    }
}
