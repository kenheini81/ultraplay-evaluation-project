﻿using Autofac;
using System.Data.Entity;
using UltraPlay.Data.Repositories;

namespace UltraPlay.Data
{
    public class UltraPlayDataModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            DbConfiguration.SetConfiguration(new UltraPlayDbConfiguration());

            builder.RegisterType<DbEntityBuilder>().SingleInstance();

            builder.RegisterType<UltraPlayDbContext>()
                   .AsSelf()
                   .As<IDbContext>()
                   .PropertiesAutowired()
                   .InstancePerLifetimeScope();

            builder.RegisterGeneric(typeof(Repository<>))
                   .AsSelf()
                   .As(typeof(IRepository<>))
                   .InstancePerLifetimeScope();
        }
    }
}
