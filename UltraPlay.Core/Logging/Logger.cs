﻿using log4net;
using System;
using System.Linq;

namespace UltraPlay.Logging
{
    public class Logger : ILogger
    {
        private readonly ILog _logger;

        internal Logger(Type type, string tenantName = "")
        {
            _logger = LogManager.GetLogger($"{tenantName} {type.FullName}");
        }

        #region Info Log
        public void Info(string message)
        {
            _logger.Info(message);
        }

        public void Information(string message)
        {
            _logger.Info(message);
        }

        #endregion

        #region Warning Log
        public void Warn(string message)
        {
            _logger.Warn(message);
        }

        public void Warn(Exception exception)
        {
            _logger.Warn(exception);
        }

        public void Warn(Exception exception, string message, params object[] format)
        {
            var formattedMessage = message;
            if (format != null && format.Any())
                formattedMessage = string.Format(formattedMessage, format);

            _logger.Warn(formattedMessage, exception);
        }
        #endregion

        #region Debug Log
        public void Debug(string message)
        {
            _logger.Debug(message);
        }
        #endregion

        #region Error Log
        public void Error(string message)
        {
            _logger.Error(message);
        }

        public void Error(Exception exception)
        {
            Error(LogUtility.BuildExceptionMessage(exception));
        }

        public void Error(Exception exception, string message, params object[] format)
        {
            var formattedMessage = message;
            if (format != null && format.Any())
                formattedMessage = string.Format(formattedMessage, format);

            _logger.Error(formattedMessage, exception);
        }
        #endregion

        #region Fatal Log
        public void Fatal(string message)
        {
            _logger.Fatal(message);
        }

        public void Fatal(Exception exception)
        {
            Fatal(LogUtility.BuildExceptionMessage(exception));
        }

        public void Fatal(string message, Exception exception)
        {
            Fatal(LogUtility.BuildExceptionMessage(exception));
        }
        #endregion
    }
}