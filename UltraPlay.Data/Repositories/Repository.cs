using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using UltraPlay.Logging;

namespace UltraPlay.Data.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly IDbSet<TEntity> _dbSet;
        public ILogger Logger { get; set; }

        public Repository(IDbContext dbContext)
        {
            DbContext = dbContext;
            _dbSet = DbContext.Set<TEntity>();
            Logger = NullLogger.Instance;
        }

        public IDbSet<TEntity> DbSet => _dbSet;

        public IDbContext DbContext { get; }

        public virtual int SaveChanges()
        {
            return DbContext.SaveChanges();
        }

        #region Gets

        public virtual IQueryable<TEntity> GetAll()
        {
            return _dbSet.AsQueryable();
        }

        public virtual TEntity GetById(params object[] id)
        {
            return _dbSet.Find(id);
        }

        public virtual IQueryable<TEntity> GetByIds(object[] ids)
        {
            return ids.Select(id => GetById(id))
                .Where(x => x != null)
                .AsQueryable();
        }

        public virtual TEntity First(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbSet.FirstOrDefault(predicate);
        }

        public virtual TEntity Last(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbSet.LastOrDefault(predicate);
        }

        public virtual int Count(Expression<Func<TEntity, bool>> expression = null)
        {
            return expression == null
                ? _dbSet.Count()
                : _dbSet.Count(expression);
        }

        public virtual IQueryable<TEntity> Fetch(Expression<Func<TEntity, bool>> expression)
        {
            return _dbSet.Where(expression);
        }

        public virtual bool Any(Expression<Func<TEntity, bool>> expression)
        {
            return _dbSet.Any(expression);
        }

        #endregion

        #region Insert

        public virtual void Insert(TEntity entity)
        {
            try
            {
                _dbSet.Add(entity);
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                throw;
            }
        }
        #endregion

        #region Update

        public virtual void Update(TEntity entity)
        {
            try
            {
                var entityEntry = DbContext.Entry(entity);

                if (entityEntry.State == EntityState.Detached)
                {
                    _dbSet.Attach(entity);
                }

                entityEntry.State = EntityState.Modified;
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                throw;
            }
        }

        public virtual void Update(Expression<Func<TEntity, bool>> predicate, Action<TEntity> dispatch)
        {
            var result = Fetch(predicate);

            foreach (var item in result)
            {
                dispatch(item);
                Update(item);
            }
        }

        #endregion

        #region Insert or update

        public virtual void InsertOrUpdate(Expression<Func<TEntity, object>> identifierExpression, TEntity entity)
        {
            _dbSet.AddOrUpdate(identifierExpression, entity);
        }

        public virtual void InsertOrUpdate(Expression<Func<TEntity, object>> identifierExpression, IEnumerable<TEntity> entities)
        {
            var entitiesToInsert = entities.ToArray();

            _dbSet.AddOrUpdate(identifierExpression, entitiesToInsert);
        }

        #endregion

        #region Delete

        public virtual void DeleteByCondition(Expression<Func<TEntity, bool>> predicate)
        {
            var result = Fetch(predicate);

            foreach (var item in result)
            {
                Delete(item);
            }
        }

        public virtual void Delete(TEntity entity)
        {
            _dbSet.Remove(entity);
        }

        #endregion

        public void Dispose()
        {
            DbContext.SaveChanges();
        }
    }
}