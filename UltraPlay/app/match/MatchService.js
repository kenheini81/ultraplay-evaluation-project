/**
 * Created on 8/7/2015.
 */
class MatchService {
    constructor($http, $config) {
        this.$http = $http;
        this.config = $config;
    }

    getMatchData(matchId, callback) {
        var _this = this;
        var url = _this.config.apis.getMatch + matchId;

        _this.$http({
            url: url,
            type: 'get'
        })
            .success(callback)
            .then(function (error) {

            });
    }
}

MatchService.$inject = ['$http', '$config'];

angular.module('ultraplay')
    .service('MatchService', MatchService);