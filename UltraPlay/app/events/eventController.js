/**
 * Created on 8/6/2015.
 */
import './EventService';

class EventController {
    constructor(EventService, $stateParams, $state) {
        this.data = [];
        this.eventName = "";
        this.sportId = $stateParams.eventId;
        this.EventService = EventService;
        this.refresh();
    }

    refresh() {
        this.EventService.getEvent(this.sportId, this.refreshEventData.bind(this));
    }

    refreshEventData(data) {
        this.eventName = data.Name;
        this.data = data.Matches;
    }
}

EventController.$inject = ['EventService', '$stateParams','$state'];

angular.module('ultraplay')
    .controller('EventController', EventController);