﻿using NUnit.Framework;
using System;
using UltraPlay.Data.Models;
using XmlSerializationHelper = UltraPlay.TestHelpers.XmlSerializationHelper;

namespace UltraPlay.Data.Tests.Models
{
    [TestFixture]
    public class TestMatchSerialization
    {
        const string XmlString = "<Match Name=\"MatchName\" ID=\"1\" StartDate=\"2015-08-02T07:00:00\" MatchType=\"PreMatch\" />";

        [Test(Description = "Serialize model object to XML should be proceeded properly")]
        public void MatchModel_ShouldBeSerializedAsXml()
        {
            var model = new Match
            {
                Name = "MatchName",
                Id = 1,
                StartDate = new DateTime(2015, 8, 2, 7, 0, 0),
                MatchType = "PreMatch"
            };

            var actualString = XmlSerializationHelper.SerializeToXmlString(model);
            Console.WriteLine("Serialized object: " + actualString);
            Assert.AreEqual(XmlString, actualString);
        }

        [Test(Description = "Deserialized given XML to modle should be proceeded properly")]
        public void MatchModel_ShouldBeDeserializedFromXml()
        {
            var model = new Match
            {
                Name = "MatchName",
                Id = 1,
                StartDate = new DateTime(2015, 8, 2, 7, 0, 0),
                MatchType = "PreMatch"
            };

            var actualObject = XmlSerializationHelper.Deserialized<Match>(XmlString);

            Assert.AreEqual(model.Name, actualObject.Name);
            Assert.AreEqual(model.Id, actualObject.Id);
            Assert.AreEqual(model.StartDate, actualObject.StartDate);
            Assert.AreEqual(model.MatchType, actualObject.MatchType);
        }


        [Test]
        public void MatchModelWithBetData_ShouldBeDeserializedFromXml()
        {
            var xmlStr =
                "<Match Name=\"MatchName\" ID=\"1\" StartDate=\"2015-08-04T22:30:00\" MatchType=\"PreMatch\">" +
                "	<Bet Name=\"Match Odds\" ID=\"2\" IsLive=\"false\">" +
                "		<Odd Name=\"1\" ID=\"3\" Value=\"1.80\" />" +
                "		<Odd Name=\"2\" ID=\"4\" Value=\"1.80\" />" +
                "	</Bet>" +
                "	<Bet Name=\"Odd/Even Games\" ID=\"3\" IsLive=\"false\">" +
                "		<Odd Name=\"Odd\" ID=\"5\" Value=\"1.45\" />" +
                "		<Odd Name=\"Even\" ID=\"6\" Value=\"2.40\" />" +
                "	</Bet>" +
                "</Match>";

            var actualObject = XmlSerializationHelper.Deserialized<Match>(xmlStr);

            Assert.AreEqual("MatchName", actualObject.Name);
            Assert.AreEqual(new DateTime(2015, 8, 4, 22, 30, 00), actualObject.StartDate);
            Assert.AreEqual(2, actualObject.Bets.Count);
            Assert.AreEqual(2, actualObject.Bets[0].Odds.Count);
            Assert.AreEqual(2, actualObject.Bets[1].Odds.Count);
            Assert.AreEqual("2", actualObject.Bets[0].Odds[1].Name);
            Assert.AreEqual(4, actualObject.Bets[0].Odds[1].Id);
            Assert.AreEqual(1.8, actualObject.Bets[0].Odds[1].Value);
        }

    }
}
