﻿using System;

namespace UltraPlay.Logging
{
    /// <summary>
    /// Represents the fake logger for Logger Injection
    /// </summary>
    public class NullLogger
    {
        public static readonly ILogger Instance;

        static NullLogger()
        {
            Instance = new Logger(typeof(NullLogger));
        }

        public static ILogger GetLogger(object instance, string tenantName = "")
        {
            var type = instance.GetType();

            while (type.BaseType != null && type.BaseType != typeof(Object))
            {
                type = type.BaseType;
            }

            return new Logger(type, tenantName);
        }
    }
}