﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.ObjectModel;
using UltraPlay.Data.Models;
using UltraPlay.Data.Repositories;
using UltraPlay.Serialization;
using Match = UltraPlay.Data.Models.Match;
using XmlSerializationHelper = UltraPlay.TestHelpers.XmlSerializationHelper;

namespace UltraPlay.ApiProxy.Tests
{
    [TestFixture]
    public class ApiProxyHandlerTest
    {
        private Mock<ApiProxyHandler> _mock;
        private Mock<IRepository<Sport>> _mockSportRepository;
        private Mock<IRepository<Event>> _mockEventRepository;
        private Mock<IRepository<Data.Models.Match>> _mockMatchRepository;
        private Mock<IRepository<Bet>> _mockBetRepository;
        private Mock<IRepository<Odd>> _mockOddRepository;
        private Mock<IXmlSerializationHelper> _mockXmlSerializationHelper;
        private ApiProxyHandler _sut;

        [SetUp]
        public void SetUp()
        {
            _mockXmlSerializationHelper = new Mock<IXmlSerializationHelper>();

            _mockSportRepository = new Mock<IRepository<Sport>>();
            _mockEventRepository = new Mock<IRepository<Event>>();
            _mockMatchRepository = new Mock<IRepository<Match>>();
            _mockBetRepository = new Mock<IRepository<Bet>>();
            _mockOddRepository = new Mock<IRepository<Odd>>();

            _mockXmlSerializationHelper.Setup(x => x.Deserialized<XmlSport>(It.IsAny<string>()))
                .Returns((string someString) => XmlSerializationHelper.Deserialized<XmlSport>(someString));

            _mock = new Mock<ApiProxyHandler>(_mockXmlSerializationHelper.Object,
                _mockSportRepository.Object,
                _mockEventRepository.Object,
                _mockMatchRepository.Object,
                _mockBetRepository.Object,
                _mockOddRepository.Object)
            {
                CallBase = true
            };
            _sut = _mock.Object;
        }


        [Test(Description = "Should call ParseData method to parse XML to Object when OnDataDownloaded method is invoked")]
        public void OnDataDownloadedTest_ShouldCallParseDataToParseToObject()
        {
            var str = "any_string";
            _mock.Setup(x => x.ParseData(It.IsAny<string>())).Returns((string someString) => new XmlSport
            {
                Sports = new Collection<Sport>()
            });
            _sut.OnDataDownloaded(str);
            _mock.Verify(x => x.ParseData(str));
        }

        [Test(Description = "Should call XmlSerializer to parse data to object")]
        public void ParseDataTest_ShouldBeAbleToParseDataIntoObject()
        {
            var input = "<?xml version=\"1.0\"?>" +
                        "<XmlSports xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" CreateDate=\"2015-08-03T06:28:15\">" +
                        "	<Sport Name=\"Sport1\" ID=\"1\">" +
                        "		<Event Name=\"Event1Sport1\" ID=\"1\" CategoryID=\"1\" IsLive=\"false\">" +
                        "			<Match Name=\"MatchOfEvent1Sport1\" ID=\"1\" StartDate=\"2015-08-02T07:00:00\" MatchType=\"PreMatch\" />" +
                        "		</Event>" +
                        "	</Sport>" +
                        "	<Sport Name=\"Sport2\" ID=\"2\">" +
                        "		<Event Name=\"Event1Sport2\" ID=\"2\" CategoryID=\"2\" IsLive=\"false\">" +
                        "			<Match Name=\"MatchOfEvent1Sport2\" ID=\"1\" StartDate=\"2015-08-04T22:30:00\" MatchType=\"PreMatch\">" +
                        "				<Bet Name=\"Match Odds\" ID=\"2\" IsLive=\"false\">" +
                        "					<Odd Name=\"1\" ID=\"3\" Value=\"1.80\" />" +
                        "					<Odd Name=\"2\" ID=\"4\" Value=\"1.80\" />" +
                        "				</Bet>" +
                        "				<Bet Name=\"Odd/Even Games\" ID=\"3\" IsLive=\"false\">" +
                        "					<Odd Name=\"Odd\" ID=\"5\" Value=\"1.45\" />" +
                        "					<Odd Name=\"Even\" ID=\"6\" Value=\"2.40\" />" +
                        "				</Bet>" +
                        "			</Match>" +
                        "		</Event>" +
                        "	</Sport>" +
                        "</XmlSports>";

            var actual = _sut.ParseData(input);

            _mockXmlSerializationHelper.Verify(x => x.Deserialized<XmlSport>(input));

            Assert.AreEqual(2, actual.Sports.Count);
            Assert.AreEqual(new DateTime(2015, 8, 3, 6, 28, 15), actual.CreateDate);
            var match = actual.Sports[1].Events[0].Matches[0];
            Assert.AreEqual("MatchOfEvent1Sport2", match.Name);
            Assert.AreEqual(2, match.Bets.Count);
            Assert.AreEqual(2, match.Bets[0].Odds.Count);
            Assert.AreEqual(2, match.Bets[1].Odds.Count);
        }
    }
}
