﻿using Autofac;
using Autofac.Core;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UltraPlay.Contexts;

namespace UltraPlay
{
    public class AutofacContainerConfigure
    {
        public static IContainer BuildContainer(IEnumerable<Assembly> inputAssemblies)
        {
            var containerBuilder = new ContainerBuilder();

            var assembliesArray = inputAssemblies.ToArray();

            var allTypes = assembliesArray.SelectMany(x => x.GetExportedTypes())
                                          .Where(t => !t.IsAbstract && !t.IsInterface)
                                          .ToArray();

            RegisterModule(allTypes.Where(typeof(IModule).IsAssignableFrom), containerBuilder);
            RegisterDependencies(allTypes.Where(typeof(IDependency).IsAssignableFrom), containerBuilder);

            var backgroundContext = new UltraPlayBackgroundContext();

            containerBuilder.RegisterInstance(backgroundContext)
                .As<UltraPlayBackgroundContext>().SingleInstance();

            containerBuilder.RegisterControllers(assembliesArray);
            containerBuilder.RegisterApiControllers(assembliesArray);

            var applicationContainer = containerBuilder.Build();

            backgroundContext.LifetimeScope = applicationContainer.BeginLifetimeScope()
                                                                  .BeginLifetimeScope();

            return applicationContainer;
        }

        private static void RegisterModule(IEnumerable<Type> modules, ContainerBuilder containerBuilder)
        {
            foreach (var module in modules)
            {
                try
                {
                    var m = Activator.CreateInstance(module) as IModule;
                    if (m != null)
                        containerBuilder.RegisterModule(m);
                }
                catch (Exception)
                {

                }
            }
        }

        private static void RegisterDependencies(IEnumerable<Type> dependencies, ContainerBuilder containerBuilder)
        {
            foreach (var dependency in dependencies)
            {
                var register = containerBuilder.RegisterType(dependency).AsSelf();

                var interfaces = dependency.GetInterfaces();

                foreach (var itf in interfaces)
                {
                    register = register.As(itf);
                    if (typeof(ISingletonDependency).IsAssignableFrom(itf))
                        register = register.SingleInstance();

                    else if (typeof(IUnitOfWorkDependency).IsAssignableFrom(itf))
                        register = register.InstancePerLifetimeScope();

                    else
                        register = register.InstancePerDependency();
                }
            }
        }
    }
}