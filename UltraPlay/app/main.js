﻿import './appBootstrap';
import './main/mainController';
import './sports/SportController';
import './events/EventController';
import './match/MatchController';
import config from './config';


angular.module('ultraplay')
    .constant('$config', config)

    .config(applicationConfig);

function applicationConfig($stateProvider, $urlRouterProvider, $httpProvider) {
    $stateProvider
        .state('home', {
            url: '/',
            templateUrl: 'app/main/main.html',
            controller: 'mainController',
            controllerAs: 'vm'
        })

        .state('sport_detail', {
            url: "/sports/:sport_id",
            templateUrl: "app/sports/index.html",
            controller: 'SportController',
            controllerAs: 'vm'
        })

        .state('sport_detail.event_detail', {
            url: "/events/:eventId",
            templateUrl: "app/events/event.html",
            controller: 'EventController',
            controllerAs: 'vm'
        })

        .state('sport_detail.event_detail.match', {
            url: "/matches/:matchId",
            templateUrl: "app/match/match.detail.html",
            controller: 'MatchController',
            controllerAs: 'vm'
        });

    $urlRouterProvider.otherwise('/');
}