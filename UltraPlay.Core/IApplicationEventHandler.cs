﻿namespace UltraPlay
{
    public interface IApplicationEventHandler : ISingletonDependency
    {
        void OnApplicationStarted();
    }
}