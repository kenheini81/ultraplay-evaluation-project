﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace UltraPlay.Data.Models
{
    [DataContract]
    public class Sport
    {
        [XmlAttribute(AttributeName = "Name"), JsonProperty]
        public string Name { get; set; }

        [XmlAttribute(AttributeName = "ID"), JsonProperty]
        [DatabaseGenerated(DatabaseGeneratedOption.None), Key]
        public long Id { get; set; }

        [XmlElement("Event"), JsonProperty]
        public virtual Collection<Event> Events { get; set; }
    }
}