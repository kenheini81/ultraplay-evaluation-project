namespace UltraPlay.Data.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class InitializeDatabase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Sports",
                c => new
                {
                    Id = c.Long(nullable: false),
                    Name = c.String(),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.Events",
                c => new
                {
                    Id = c.Long(nullable: false),
                    Name = c.String(),
                    CategoryId = c.Long(nullable: false),
                    IsLive = c.Boolean(nullable: false),
                    Sport_Id = c.Long(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sports", t => t.Sport_Id, cascadeDelete: true)
                .Index(t => t.Sport_Id);

            CreateTable(
                "dbo.Matches",
                c => new
                {
                    Id = c.Long(nullable: false),
                    Name = c.String(),
                    StartDate = c.DateTime(nullable: false),
                    MatchType = c.String(),
                    Event_Id = c.Long(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Events", t => t.Event_Id, cascadeDelete: true)
                .Index(t => t.Event_Id);

            CreateTable(
                "dbo.Bets",
                c => new
                {
                    Id = c.Long(nullable: false),
                    Name = c.String(),
                    IsLive = c.Boolean(nullable: false),
                    Match_Id = c.Long(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Matches", t => t.Match_Id, cascadeDelete: true)
                .Index(t => t.Match_Id);

            CreateTable(
                "dbo.Odds",
                c => new
                {
                    Id = c.Long(nullable: false),
                    Name = c.String(),
                    Value = c.Double(nullable: false),
                    Bet_Id = c.Long(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Bets", t => t.Bet_Id, cascadeDelete: true)
                .Index(t => t.Bet_Id);

        }

        public override void Down()
        {
            DropForeignKey("dbo.Events", "Sport_Id", "dbo.Sports");
            DropForeignKey("dbo.Matches", "Event_Id", "dbo.Events");
            DropForeignKey("dbo.Odds", "Bet_Id", "dbo.Bets");
            DropForeignKey("dbo.Bets", "Match_Id", "dbo.Matches");
            DropIndex("dbo.Odds", new[] { "Bet_Id" });
            DropIndex("dbo.Bets", new[] { "Match_Id" });
            DropIndex("dbo.Matches", new[] { "Event_Id" });
            DropIndex("dbo.Events", new[] { "Sport_Id" });
            DropTable("dbo.Odds");
            DropTable("dbo.Bets");
            DropTable("dbo.Matches");
            DropTable("dbo.Events");
            DropTable("dbo.Sports");
        }
    }
}
